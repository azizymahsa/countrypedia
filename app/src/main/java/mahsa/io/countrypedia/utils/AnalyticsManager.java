package mahsa.io.countrypedia.utils;

import android.app.Application;

import timber.log.Timber;

/**
 * Created by azizymahsa@gmail.com  on 20.02.19.
 */
public class AnalyticsManager {

    private Application app;

    public AnalyticsManager(Application app) {
        this.app = app;
    }

    public void logScreenView(String screenName) {
        Timber.d("Logged screen name: " + screenName);
    }
}

