package mahsa.io.countrypedia.utils;

import rx.Observer;

/**
 * Created by azizymahsa@gmail.com  on 20.02.19.
 */
public class SimpleObserver<T> implements Observer<T> {
    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onNext(T t) {

    }
}