package mahsa.io.countrypedia.utils;

import android.text.TextUtils;

/**
 * Created by azizymahsa@gmail.com  on 20.02.19.
 */
public class Validator {

    public Validator() {
    }

    public boolean validUsername(String username) {
        return !TextUtils.isEmpty(username);
    }
}
