package mahsa.io.countrypedia.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by azizymahsa@gmail.com
 */
public class ResponseGetCountry {
    public long capital;
    public String name;


    @GET("all")
    Call<List<ResponseGetCountry>> getCountries();
}
