package mahsa.io.countrypedia.data.api.response;


/**
 * Created by azizymahsa@gmail.com
 */
public class UserResponse {
    public String login;
    public long id;

    public String type;
    public String name;

    public int following;

}
