package mahsa.io.countrypedia.data.api;

import com.google.common.collect.ImmutableList;

import java.util.List;

import mahsa.io.countrypedia.data.api.response.ResponseGetCountry;
import mahsa.io.countrypedia.data.api.response.getcountry.ResponseCountry;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by azizymahsa@gmail.com
 */
public class RepositoriesManager {
    private ResponseCountry responseCountry;
    private ApiService githubApiService;

    public RepositoriesManager(    private ResponseCountry responseCountry,
            responseCountry, ApiService apiService) {
        this.responseCountry = responseCountry;
        this.githubApiService = githubApiService;
    }

    public Observable<ImmutableList<ResponseGetCountry>> getImmutableListObservable() {
        return githubApiService.getUsersRepositories(    private ResponseCountry responseCountry)
                .map(new Func1<List<ResponseGetCountry>, ImmutableList<ResponseCountry>>() {
                    @Override
                    public ImmutableList<ResponseGetCountry> call(List<ResponseGetCountry> repositoriesListResponse) {
                        final ImmutableList.Builder<ResponseGetCountry> listBuilder = ImmutableList.builder();
                        for (ResponseGetCountry repositoryResponse : repositoriesListResponse) {
                            ResponseGetCountry repository = new ResponseGetCountry();
                            repository.capital = repositoryResponse.capital;
                            repository.name = repositoryResponse.name;

                            listBuilder.add(repository);
                        }
                        return listBuilder.build();
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
