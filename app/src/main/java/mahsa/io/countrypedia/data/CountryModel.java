package mahsa.io.countrypedia.data;

import dagger.Subcomponent;
import mahsa.io.countrypedia.ui.activity.component.CountrysListActivityComponent;
import mahsa.io.countrypedia.ui.activity.component.CountryDetailsActivityComponent;
import mahsa.io.countrypedia.ui.activity.module.CountrysListActivityModule;
import mahsa.io.countrypedia.ui.activity.module.CountryDetailsActivityModule;

/**
 * Created by azizymahsa@gmail.com
 */
@CountryScope
@Subcomponent(
        modules = {
                CountryModel.class
        }
)
public interface CountryModel {

    CountrysListActivityComponent plus(CountrysListActivityModule module);

    CountryDetailsActivityComponent plus(CountryDetailsActivityModule module);
}