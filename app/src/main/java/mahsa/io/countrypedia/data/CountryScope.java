package mahsa.io.countrypedia.data;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by azizymahsa@gmail.com  on  22.04.15.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface CountryScope {
}