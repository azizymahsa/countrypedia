package mahsa.io.countrypedia.data.api;

import java.util.List;

import mahsa.io.countrypedia.data.api.request.RequestGetCountry;
import mahsa.io.countrypedia.data.api.response.ResponseGetCountry;
import mahsa.io.countrypedia.data.api.response.UserResponse;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by azizymahsa@gmail.com  on  22.04.15.
 */
public interface ApiService {

    @GET("/users/{username}")
    Observable<UserResponse> getUser(
            @Path("username") String username
    );

    //@GET("/users/{username}/repos")
    @GET("/GetAirportWithParentsWithCulture")
    Observable<List<ResponseGetCountry>> getUsersRepositories(
            @Path("username") String username
    );

   /* @GET("yql")
    Observable<Response<WeatherApi>> yahooWeather(
            @Query("q") String query,
            @Query("format") String format
    );*/

    //*************NEW**********************************************************************************************************
    //*************package*************
    @Headers("Content-Type: application/json")
    @POST("/GetAirportWithParentsWithCulture")
    Observable<Response<List<ResponseGetCountry>>> getResponseObservable(
            @Body RequestGetCountry requestGetCountry
    );
}
