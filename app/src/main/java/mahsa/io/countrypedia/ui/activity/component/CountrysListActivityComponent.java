package mahsa.io.countrypedia.ui.activity.component;

import dagger.Subcomponent;
import mahsa.io.countrypedia.ui.activity.ActivityScope;
import mahsa.io.countrypedia.ui.activity.CountrysListActivity;
import mahsa.io.countrypedia.ui.activity.module.CountrysListActivityModule;

/**
 * Created by azizymahsa@gmail.com  on 20.02.19.
 */
@ActivityScope
@Subcomponent(
        modules = CountrysListActivityModule.class
)
public interface CountrysListActivityComponent {

    CountrysListActivity inject(CountrysListActivity repositoriesListActivity);

}