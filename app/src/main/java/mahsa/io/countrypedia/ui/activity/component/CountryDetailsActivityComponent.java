package mahsa.io.countrypedia.ui.activity.component;

import dagger.Subcomponent;
import mahsa.io.countrypedia.ui.activity.ActivityScope;
import mahsa.io.countrypedia.ui.activity.CountrysDetailsActivity;
import mahsa.io.countrypedia.ui.activity.module.CountryDetailsActivityModule;

/**
 * Created by azizymahsa@gmail.com  on 20.02.19.
 */
@ActivityScope
@Subcomponent(
        modules = CountryDetailsActivityModule.class
)
public interface CountryDetailsActivityComponent {

    CountrysDetailsActivity inject(CountrysDetailsActivity repositoryDetailsActivity);

}