package mahsa.io.countrypedia.ui.activity.module;

import android.support.v7.widget.LinearLayoutManager;

import java.util.Map;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntKey;
import dagger.multibindings.IntoMap;
import mahsa.io.countrypedia.data.api.RepositoriesManager;
import mahsa.io.countrypedia.ui.activity.ActivityScope;
import mahsa.io.countrypedia.ui.activity.CountrysListActivity;
import mahsa.io.countrypedia.ui.activity.presenter.CountrysListActivityPresenter;
import mahsa.io.countrypedia.ui.adapter.RepositoriesListAdapter;
import mahsa.io.countrypedia.ui.adapter.viewholder.RepositoriesListViewHolderFactory;
import mahsa.io.countrypedia.ui.adapter.viewholder.RepositoryViewHolderBigFactory;
import mahsa.io.countrypedia.ui.adapter.viewholder.RepositoryViewHolderFeaturedFactory;
import mahsa.io.countrypedia.ui.adapter.viewholder.RepositoryViewHolderNormalFactory;

/**
 * Created by azizymahsa@gmail.com  on 20.02.19.
 */
@Module
public class CountrysListActivityModule {
    private CountrysListActivity repositoriesListActivity;

    public CountrysListActivityModule(CountrysListActivity repositoriesListActivity) {
        this.repositoriesListActivity = repositoriesListActivity;
    }

    @Provides
    @ActivityScope
    CountrysListActivity provideRepositoriesListActivity() {
        return repositoriesListActivity;
    }

    @Provides
    @ActivityScope
    CountrysListActivityPresenter provideRepositoriesListActivityPresenter(RepositoriesManager repositoriesManager) {
        return new CountrysListActivityPresenter(repositoriesListActivity, repositoriesManager);
    }

    @Provides
    @ActivityScope
    RepositoriesListAdapter provideRepositoriesListAdapter(CountrysListActivity repositoriesListActivity,
                                                           Map<Integer, RepositoriesListViewHolderFactory> viewHolderFactories) {
        return new RepositoriesListAdapter(repositoriesListActivity, viewHolderFactories);
    }

    @Provides
    @ActivityScope
    LinearLayoutManager provideLinearLayoutManager(CountrysListActivity repositoriesListActivity) {
        return new LinearLayoutManager(repositoriesListActivity);
    }

    @Provides
    @IntoMap
    @IntKey(Repository.TYPE_NORMAL)
    RepositoriesListViewHolderFactory provideViewHolderNormal() {
        return new RepositoryViewHolderNormalFactory();
    }

    @Provides
    @IntoMap
    @IntKey(Repository.TYPE_BIG)
    RepositoriesListViewHolderFactory provideViewHolderBig() {
        return new RepositoryViewHolderBigFactory();
    }

    @Provides
    @IntoMap
    @IntKey(Repository.TYPE_FEATURED)
    RepositoriesListViewHolderFactory provideViewHolderFeatured() {
        return new RepositoryViewHolderFeaturedFactory();
    }
}