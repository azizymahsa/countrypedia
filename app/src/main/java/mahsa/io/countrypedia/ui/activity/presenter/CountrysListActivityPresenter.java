package mahsa.io.countrypedia.ui.activity.presenter;

import com.google.common.collect.ImmutableList;

import mahsa.io.countrypedia.data.api.RepositoriesManager;
import mahsa.io.countrypedia.ui.activity.CountrysListActivity;
import mahsa.io.countrypedia.utils.SimpleObserver;

/**
 * Created by azizymahsa@gmail.com  on 20.02.19.
 */
public class CountrysListActivityPresenter {
    private CountrysListActivity repositoriesListActivity;
    private RepositoriesManager repositoriesManager;

    public CountrysListActivityPresenter(CountrysListActivity repositoriesListActivity,
                                         RepositoriesManager repositoriesManager) {
        this.repositoriesListActivity = repositoriesListActivity;
        this.repositoriesManager = repositoriesManager;
    }

    public void loadRepositories() {
        repositoriesListActivity.showLoading(true);
        repositoriesManager.getUsersRepositories().subscribe(new SimpleObserver<ImmutableList<Repository>>() {
            @Override
            public void onNext(ImmutableList<Repository> repositories) {
                repositoriesListActivity.showLoading(false);
                repositoriesListActivity.setRepositories(repositories);
            }

            @Override
            public void onError(Throwable e) {
                repositoriesListActivity.showLoading(false);
            }
        });
    }

}
