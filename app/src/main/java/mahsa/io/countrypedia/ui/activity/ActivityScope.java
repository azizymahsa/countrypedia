package mahsa.io.countrypedia.ui.activity;

import javax.inject.Scope;

/**
 * Created by azizymahsa@gmail.com  on  22.04.15.
 */
@Scope
public @interface ActivityScope {
}