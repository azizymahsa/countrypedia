package mahsa.io.countrypedia.ui.activity.module;

import dagger.Module;
import dagger.Provides;
import mahsa.io.countrypedia.data.api.response.getcountry.ResponseCountry;
import mahsa.io.countrypedia.ui.activity.ActivityScope;
import mahsa.io.countrypedia.ui.activity.CountrysDetailsActivity;
import mahsa.io.countrypedia.ui.activity.presenter.CountrysDetailsActivityPresenter;

/**
 * Created by azizymahsa@gmail.com  on 20.02.19.
 */
@Module
public class CountryDetailsActivityModule {
    private CountrysDetailsActivity repositoryDetailsActivity;

    public CountryDetailsActivityModule(CountrysDetailsActivity repositoryDetailsActivity) {
        this.repositoryDetailsActivity = repositoryDetailsActivity;
    }

    @Provides
    @ActivityScope
    CountrysDetailsActivity provideRepositoryDetailsActivity() {
        return repositoryDetailsActivity;
    }

    @Provides
    @ActivityScope
    CountrysDetailsActivityPresenter provideRepositoryDetailsActivityPresenter(ResponseCountry user) {
        return new CountrysDetailsActivityPresenter(repositoryDetailsActivity, user);
    }
}