package mahsa.io.countrypedia.ui.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * Created by azizymahsa@gmail.com  on  11.06.2016.
 */

public interface RepositoriesListViewHolderFactory {
    RecyclerView.ViewHolder createViewHolder(ViewGroup parent);
}
