package mahsa.io.countrypedia.ui.activity.component;

import dagger.Subcomponent;
import mahsa.io.countrypedia.ui.activity.ActivityScope;
import mahsa.io.countrypedia.ui.activity.SplashActivity;
import mahsa.io.countrypedia.ui.activity.module.SplashActivityModule;

/**
 * Created by azizymahsa@gmail.com  on 20.02.19.
 */
@ActivityScope
@Subcomponent(
        modules = SplashActivityModule.class
)
public interface SplashActivityComponent {

    SplashActivity inject(SplashActivity splashActivity);

}