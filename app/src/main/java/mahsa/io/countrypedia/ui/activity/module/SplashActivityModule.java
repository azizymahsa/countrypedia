package mahsa.io.countrypedia.ui.activity.module;

import dagger.Module;
import dagger.Provides;
import mahsa.io.countrypedia.HeavyLibraryWrapper;
import mahsa.io.countrypedia.ui.activity.ActivityScope;
import mahsa.io.countrypedia.ui.activity.SplashActivity;
import mahsa.io.countrypedia.ui.activity.presenter.SplashActivityPresenter;
import mahsa.io.countrypedia.utils.Validator;

/**
 * Created by azizymahsa@gmail.com  on 20.02.19.
 */
@Module
public class SplashActivityModule {
    private SplashActivity splashActivity;

    public SplashActivityModule(SplashActivity splashActivity) {
        this.splashActivity = splashActivity;
    }

    @Provides
    @ActivityScope
    SplashActivity provideSplashActivity() {
        return splashActivity;
    }

    @Provides
    @ActivityScope
    SplashActivityPresenter
    provideSplashActivityPresenter(Validator validator, UserManager userManager, HeavyLibraryWrapper heavyLibraryWrapper) {
        return new SplashActivityPresenter(splashActivity, validator, userManager, heavyLibraryWrapper);
    }
}