package mahsa.io.countrypedia.ui.activity.presenter;

import mahsa.io.countrypedia.data.api.response.getcountry.ResponseCountry;
import mahsa.io.countrypedia.ui.activity.CountrysDetailsActivity;

/**
 * Created by azizymahsa@gmail.com  on 20.02.19.
 */
public class CountrysDetailsActivityPresenter {
    private CountrysDetailsActivity repositoryDetailsActivity;
    private ResponseCountry;

    public CountrysDetailsActivityPresenter(CountrysDetailsActivity repositoryDetailsActivity, ResponseCountry) {
        this.repositoryDetailsActivity = repositoryDetailsActivity;
        this.ResponseCountry = ResponseCountry;
    }

    public void init() {
        repositoryDetailsActivity.setupUserName(ResponseCountry);
    }
}
