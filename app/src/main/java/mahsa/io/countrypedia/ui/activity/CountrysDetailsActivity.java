package mahsa.io.countrypedia.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import mahsa.io.countrypedia.ClientApplication;
import mahsa.io.countrypedia.R;
import mahsa.io.countrypedia.data.api.response.getcountry.ResponseCountry;
import mahsa.io.countrypedia.ui.activity.module.CountryDetailsActivityModule;
import mahsa.io.countrypedia.ui.activity.presenter.CountrysDetailsActivityPresenter;
import mahsa.io.countrypedia.utils.AnalyticsManager;


public class CountrysDetailsActivity extends BaseActivity {
    private static final String ARG_REPOSITORY = "arg_repository";

    @BindView(R.id.tvRepoName)
    TextView tvRepoName;
    @BindView(R.id.tvRepoDetails)
    TextView tvRepoDetails;
    @BindView(R.id.tvUserName)
    TextView tvUserName;

    @Inject
    AnalyticsManager analyticsManager;
    @Inject
    CountrysDetailsActivityPresenter presenter;

    private ResponseCountry repository;

    public static void startWithRepository(ResponseCountry repository, Activity startingActivity) {
        Intent intent = new Intent(startingActivity, CountrysDetailsActivity.class);
        intent.putExtra(ARG_REPOSITORY, repository);
        startingActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_details);
        ButterKnife.bind(this);
        analyticsManager.logScreenView(getClass().getName());

        repository = getIntent().getParcelableExtra(ARG_REPOSITORY);
        tvRepoName.setText(repository.getName());
        tvRepoDetails.setText(repository.getCapital());

        presenter.init();
    }

    @Override
    protected void setupActivityComponent() {
        ClientApplication.get(this).getUserComponent()
                .plus(new CountryDetailsActivityModule(this))
                .inject(this);

    }

    public void setupUserName(String userName) {
        tvUserName.setText(userName);
    }
}