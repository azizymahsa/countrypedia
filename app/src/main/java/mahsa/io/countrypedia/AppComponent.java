package mahsa.io.countrypedia;

import javax.inject.Singleton;

import dagger.Component;
import mahsa.io.countrypedia.data.api.ApiModule;
import mahsa.io.countrypedia.data.CountryModel;
import mahsa.io.countrypedia.ui.activity.component.SplashActivityComponent;
import mahsa.io.countrypedia.ui.activity.module.SplashActivityModule;

/**
 * Created by azizymahsa@gmail.com  on  22.04.15.
 */
@Singleton
@Component(
        modules = {
                AppModule.class,
                ApiModule.class
        }
)
public interface AppComponent {

    SplashActivityComponent plus(SplashActivityModule module);

    CountryModel plus(UserModule userModule);

}