package mahsa.io.countrypedia;

import android.app.Application;
import android.content.Context;

import com.frogermcs.androiddevmetrics.AndroidDevMetrics;

import mahsa.io.countrypedia.data.CountryModel;
import timber.log.Timber;

/**
 * Created by azizymahsa@gmail.com  on  22.04.15.
 */
    public class ClientApplication extends Application {

    private AppComponent appComponent;
    private CountryModel userComponent;

    public static ClientApplication get(Context context) {
        return (ClientApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
            AndroidDevMetrics.initWith(this);
        }

        initAppComponent();
    }

    private void initAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public CountryModel createUserComponent(ResponseCountry responseCountry) {
        userComponent = appComponent.plus(new UserModule(responseCountry));
        return userComponent;
    }

    public void releaseUserComponent() {
        userComponent = null;
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public CountryModel getUserComponent() {
        return userComponent;
    }

}