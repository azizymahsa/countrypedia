package mahsa.io.countrypedia;

import android.app.Application;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import mahsa.io.countrypedia.utils.AnalyticsManager;

public class MockAppModule extends AppModule {
    @Mock
    AnalyticsManager analyticsManagerMock;

    public MockAppModule(Application application) {
        super(application);
        MockitoAnnotations.initMocks(this);
    }

    @Override
    AnalyticsManager provideAnalyticsManager() {
        return analyticsManagerMock;
    }
}