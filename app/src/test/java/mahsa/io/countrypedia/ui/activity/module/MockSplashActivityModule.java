package mahsa.io.countrypedia.ui.activity.module;

import dagger.Provides;
import mahsa.io.countrypedia.data.api.response.getcountry.ResponseCountry;
import mahsa.io.countrypedia.ui.activity.ActivityScope;
import mahsa.io.countrypedia.ui.activity.SplashActivity;
import mahsa.io.countrypedia.ui.activity.presenter.SplashActivityPresenter;
import mahsa.io.countrypedia.utils.Validator;

import static org.mockito.Mockito.mock;

public class MockSplashActivityModule extends SplashActivityModule {

    public MockSplashActivityModule(SplashActivity splashActivity) {
        super(splashActivity);
    }

    @Provides
    @ActivityScope
    SplashActivityPresenter provideSplashActivityPresenter(Validator validator, ResponseCountry responseCountry) {
        return mock(SplashActivityPresenter.class);
    }
}
