package mahsa.io.countrypedia;

import mahsa.io.countrypedia.ui.activity.component.SplashActivityComponent;
import mahsa.io.countrypedia.ui.activity.module.SplashActivityModule;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class TestGithubClientApplication
        extends ClientApplication {

    private AppComponent appComponent;
    private SplashActivityComponent splashActivityComponent;

    @Override
    public AppComponent getAppComponent() {
        if (appComponent == null) {
            appComponent = mock(AppComponent.class);
            when(appComponent.plus(any(SplashActivityModule.class)))
                    .thenReturn(splashActivityComponent);
        }

        return appComponent;
    }

    public void setSplashActivityComponent(SplashActivityComponent component) {
        this.splashActivityComponent = component;
    }
}