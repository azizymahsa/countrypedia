package mahsa.io.countrypedia.inject;

import mahsa.io.countrypedia.AppComponent;
import mahsa.io.countrypedia.AppModule;
import mahsa.io.countrypedia.DaggerAppComponent;
import mahsa.io.countrypedia.ClientApplication;
import mahsa.io.countrypedia.data.api.ApiModule;

/**
 * azizymahsa@gmail.com on 24.09.15.
 */
public class ApplicationMock extends ClientApplication {

    private AppComponent appComponent;
    private ApiModule githubApiModuleMock;

    public void setGithubApiModuleMock(ApiModule githubApiModuleMock) {
        this.githubApiModuleMock = githubApiModuleMock;
        setupMockAppComponent();
    }

    public void setupMockAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .githubApiModule(githubApiModuleMock)
                .build();
    }

    @Override
    public AppComponent getAppComponent() {
        return appComponent == null ? super.getAppComponent() : appComponent;
    }
}