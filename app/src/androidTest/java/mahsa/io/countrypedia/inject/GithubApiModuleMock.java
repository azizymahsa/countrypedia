package mahsa.io.countrypedia.inject;

import mahsa.io.countrypedia.data.api.ApiModule;
import mahsa.io.countrypedia.data.api.ApiService;
import mahsa.io.countrypedia.data.api.response.getcountry.ResponseCountry;

/**
 * azizymahsa@gmail.com on 23.09.15.
 */
public class GithubApiModuleMock extends ApiModule {

    private ResponseCountry responseCountry;

    public GithubApiModuleMock(ResponseCountry responseCountry) {
        this.responseCountry = responseCountry;
    }

    @Override
    public ResponseCountry provideUserManager(ApiService githubApiService) {
        return responseCountry;
    }
}
